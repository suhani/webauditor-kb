+++
title= "S02 - Site Redirecting to Another Site"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 2
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Site redirecting to another site |
| Code | S02 |
| Description | The site cannot redirect to https or changes in domain or URL paths |
| Level | Warning |


The site unable to redirect to HTTPS will have lower preference over the site with HTTPS. The changes in the domain name and URL paths affect the ranking of the site as they need to be recrawled and reindex the site. Following are reason to redirect:

  - The protocol changes — http://www.example.com to https://www.example.com
  - The domain name changes — example.com to example.net
  - The URL paths change — example.com/page.php?id=1 to example.com/widget

## How to resolve?


  - Secure your site with a certificate to achieve HTTPS.  
  - If there is a site move with URL changes, move them into smaller sections.  
  - Using 301 or 302 will not cause a loss in PageRank.  
  - Update the sitemap according to the changes to make it easier for Google bot. 