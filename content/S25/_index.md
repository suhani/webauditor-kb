+++
title= "S25 - No Compression Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 25
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | No compression found |
| Code | S25 |
| Description | Site speed is one of key factor for increase ranking. by compressing we can came one step closer to it.  |
| Level | Error |

## What is compression and why is it important for SEO?

Browser/search engines and site server do communicate using HTTP protocol, if you want to make these communication faster, you need some kind of compression like `gzip`.

All modern browsers support and automatically negotiate gzip compression for all HTTP requests. Enabling gzip compression can reduce the size of the transferred response by up to 90%, which can significantly reduce the amount of time to download the resource, reduce data usage for the client, and improve the time to first render of your pages. See [text compression with GZIP](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/optimize-encoding-and-transfer#text-compression-with-gzip) to learn more.

Compression matters for SEO because speed matters in SEO. Web users don’t like waiting for pages to load , so Google isn’t really interested in recommending slow pages to their users.

As such, how quickly Google is able to connect to your site and access content and information to a user has a direct impact on rankings.

Google is also talking site speed in ranking consideration 
[Using site speed in web search ranking](https://webmasters.googleblog.com/2010/04/using-site-speed-in-web-search-ranking.html) 
[Using page speed in mobile search ranking](https://webmasters.googleblog.com/2018/01/using-page-speed-in-mobile-search.html)

## How to enable compression for site?
Check this article to resolve this issue [Enable Compression](https://developers.google.com/speed/docs/insights/EnableCompression), 

Also you can check some rules to improve [page speed](https://developers.google.com/speed/docs/insights/rules)

# Tools to check speed
https://developers.google.com/speed/pagespeed/insights/
https://www.webpagetest.org/
