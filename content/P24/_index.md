+++
title= "P24 - Pages With Redirect"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 24
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Pages with redirect |
| Code | P24 |
| Description | Pages having no status code for redirect, multiple chaining redirects, and cloaking  |
| Level | Warning |


## Not using appropriate Status Code for every redirect

Using proper redirect status code is necessary for google crawlers to understand the reason for the redirect. Without which, it may result in downranking. If there is a permanent change in the page URL use 301 to redirect to the new URL. 

## What is Chaining redirects?
If the page redirects to another location and there finds another redirect is termed as chaining redirects. Nested redirects  But don't use chaining redirects which may result in not crawling of that page by the crawler.

## What is Cloaking and Sneaky redirects?
The redirects which are different for human users and different for google bot is termed as cloaking. Showing different output page for desktop and mobile is termed as sneaky redirects.

## How to resolve?

Using 301 states the URL move for page or domain move for the whole website. If there is confusion between the old URL and the new URL, are the changes recognized by google crawler, you can verify that by checking on [google search console](https://search.google.com/search-console/welcome). 

Don't use chaining redirects which may result in not crawling of that page by the crawler.

Do not differentiate redirects between the human user and google bot or desktop and mobile platforms. Do not check for google bot IP address anywhere.