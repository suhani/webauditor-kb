+++
title= "P20 - Internal Page Not Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 20
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Internal page not found |
| Code | P20 |
| Description | If internal page return status code 404, this alert will raise |
| Level | Error |

## What is 404 (Page Not Found) errors?
While requesting the server for a web page and if not found, it will return a 404 error response, this is called 404 errors.

404 errors won’t impact your site’s search performance, and you can safely ignore them if you’re certain that the URLs should not exist on your site. It’s important to make sure that these and other invalid URLs return a proper 404 HTTP response code, and that they are not blocked by the site’s robots.txt file.

## How to resolve?
In the case of the internal page, which will be indexed by the search engine, make sure it does not have 404 errors.

In theory, 404s have an impact on rankings. But not the rankings of a whole site. If a page returns a 404 error code, it means it doesn’t exist, so google and other search engines will not index it. Just make sure your pages exist if you want them to rank. having thousands and thousands of 404 pages can `impact your website overall`.

However, it’s not so much the actual 404 pages that hurt SEO, but the links that contain URLs pointing to the 404s. so make sure all internal links within your site exists.

Learn more about [404 errors](https://support.google.com/webmasters/answer/2445990).  
Learn about [Soft 404 errors](https://support.google.com/webmasters/answer/181708).