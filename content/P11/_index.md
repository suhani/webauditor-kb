+++
title= "P11 - Long Title Tags"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 11
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Long title tag |
| Code | P11 |
| Description | This alert occurs if the ideal length of the title greater than 60 characters |
| Level | Warning |

The title tag is a short tag that tells about page content. if you make a large title tag, then it's not helpful for users, what is the page all about, in long text search engines like google automatically truncate the text, which it might not be useful for user.

Also, google doesn't specify any length officially, but observing search results we can see that google have some length algorithm, google may crawl your full title but it shows limited character to visible to the user, which is sometimes not ideal for the user and don't get any useful information, and you can lose valuable user/customer.

Also, check this article how to make the google title tag
[Better page titles in search results](https://webmasters.googleblog.com/2012/01/better-page-titles-in-search-results.html)
[Create unique, accurate page titles](https://support.google.com/webmasters/answer/7451184#uniquepagetitles)
