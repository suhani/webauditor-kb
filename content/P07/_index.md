---
title: P07 - Duplicate Meta Description
date: 2020-04-21T09:13:28.000Z
description: description
draft: false
weight: 7
---
| Alert Properties | Alert Description                                     |
| ---------------- | ----------------------------------------------------- |
| Alert Name       | Duplicate meta description                            |
| Code             | P07                                                   |
| Description      | These alert occurs when page description is duplicate |
| Level            | Warning                                               |

## What is page description(meta description tag) ?

A page's description meta tag gives search engines a summary of what the page is about. 

## Avoid duplicate meta description

Identical or similar descriptions on every page of a site aren't helpful when individual pages appear in the web results. 
In these cases search engines like google `less likely to display` the boilerplate text. Wherever possible, create descriptions that accurately describe the specific page. Use site-level descriptions on the main home page or other aggregation pages, and use page-level descriptions everywhere else. If you don't have time to create a description for every single page, try to prioritize your content: At the very least, create a description for the critical URLs like your home page and popular pages.  

To know about meta duplicate page read this article [How to write meta description?](https://support.google.com/webmasters/answer/35624?hl=en)