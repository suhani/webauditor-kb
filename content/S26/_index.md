+++
title= "S26 - Google Bot Blocked By Robots.txt"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 26
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Google bot blocked by robots.txt |
| Code | S26 |
| Description | The whole or part of the website is blocked for Google bot to access by robots.txt  |
| Level | Error |


## What is Google bot?
Google bot is the web crawler that will crawl your website. Blocking google bot will affect the overall ranking of the site as the crawler will not be able to index the blocked content.

## What is Robots.txt?

Robots.txt file controls which pages or files should the Google bot access. It reduces the extra overloading of the site with requests, however, it cannot be used to keep web pages out of Google. To do so, one may use noindex directives, or password-protect your page.

## What is Robots.txt used for?

Robots.txt is used for traffic management of Web pages, media files, and resource files. Some of the media files and resource files can be blocked such as unimportant images, script, or style files, video, and audio files.


## How to unblock Google bot from robots.txt?
If the Google bot is blocked or all the user-agent are blocked then it may look like this:
```
User-agent: *
Disallow: /
OR
User-agent: Googlebot
Disallow: /
```
To allow Google bot or all user-agent change robots.txt like this:
```
User-agent: *
Allow: /
OR
User-agent: Googlebot
Allow: /
```