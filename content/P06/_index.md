---
title: P06 - Duplicate Titles
date: 2020-05-19T14:00:11.000Z
description: description
draft: false
weight: 6
---
| Alert Properties | Alert Description                                          |
| ---------------- | ---------------------------------------------------------- |
| Alert Name       | Duplicate titles                                           |
| Code             | P06                                                        |
| Description      | This alert occurs when multiple pages have the same title. |
| Level            | Warning                                                    |

## What is a title tag?

The title tag is the identity of the web page. titles are critical to giving users a quick insight into the content of a result and why it’s relevant to their query. It's often the primary piece of information used to decide which result to click on, so it's important to use high-quality titles on your web pages.

## Avoid Duplicate title tag.

It's important to have unique, descriptive titles for each page on your site. 

A duplicate title is not bad but google or any search engine will not understand which page has high priority compared to others, which page needs to be picked, in that case, add a canonical tag, so search engine can easily understand which page title to pick. `<link rel="canonical" href="www.example.com/products" />`

To know about title page read this article [Create good titles and snippets in Search Results](https://support.google.com/webmasters/answer/35624?hl=en)

Check this video [How does Google choose titles for search results?](https://www.youtube.com/watch?v=L3HX_8BAhB4)