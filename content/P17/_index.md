+++
title= "P17 - Allow By Robots.txt But Have Noindex Meta"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 17
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Allowed by robots.txt but have noindex meta |
| Code | P17 |
| Description | If the page have nofollow meta tag then this alert raise  |
| Level | Warning |

If you want that search engine like google will not include the page in search results you can use noindex meta tag, a search engine like google still can crawl the page. if you want to completely block the crawling and index of the page, you should put it in robots.txt using `Disallow`rule.

`robots.txt`
```
User-agent: *
Disallow: /products/
```

#### How to resolve?
Ideally, you should not block the search engine to the index page, this will block present in the search results. so before adding the noindex page, make sure you have clear thoughts about what you blocking.

Learn about noindex
[Block search indexing with 'noindex'](https://support.google.com/webmasters/answer/93710)

#### Why can't I use robots.txt to block my file by google?
If you use a robots.txt file on your website, you can tell Google not to crawl a page. However, if Google finds a link to your page on another site, with descriptive text, we might generate a search result from that. If you have included a “noindex” tag on the page, Google won’t see it, because Google must crawl (fetch) the page in order to see that tag, but Google won’t fetch your page if there’s a robots.txt file blocking it! Therefore, you should let Google crawl the page and see the “noindex” tag or header. It sounds counterintuitive, but you need to let Google try to fetch the page and fail (because of password protection) or see the “noindex” tag to ensure it’s omitted from search results.

This does not apply to images; for images, robots.txt is the correct way to block images from search results.
