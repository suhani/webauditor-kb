+++
title= "P27 - Google Bot Blocked By Meta Tags"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 27
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Google bot blocked by meta tags |
| Code | P27 |
| Description | The pages have been blocked for google bot by the meta tags |
| Level | Error |

## What is Google bot?
Google bot is the web crawler that will crawl your website. Blocking google bot will affect the overall ranking of the site as the crawler will not be able to index the blocked content.

## How to resolve?
Do not use password-protected files, blocking pages, images, noindex directives, opting-out, use of nosnippet in the meta tag.
Remove meta tags like:
```
//Blocking googlebot
<meta name="googlebot" content="noindex, nofollow">
<meta name="googlebot" content="unavailable_after: 25-Aug-2011 15:00:00 EST">

//Blocking robots from crawling
<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noimageindex">
```

Read more about [Google bot](https://support.google.com/webmasters/answer/182072)
Read more about [Blocked content](https://support.google.com/news/publisher-center/answer/9605477?hl=en)
```
end of article ^^
```

Google Webmaster
https://support.google.com/webmasters/answer/2387297?hl=en&ref_topic=9427949
https://support.google.com/webmasters/answer/182072
https://support.google.com/news/publisher-center/answer/9605477?hl=en