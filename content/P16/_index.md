+++
title= "P16 - Allowed By Robots.txt But Have Nofollow Meta"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 16
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Allowed by robots.txt but have nofollow meta |
| Code | P16 |
| Description | If the page have nofollow meta tag then this alert raise  |
| Level | Warning |

## How to resolve ?
If you want that search engine like google will not follow links present in the page `nofollow` meta tag is useful, but this will also nofollow internal links present in the page which is not good for SEO, if you want to tell search engine that nofollow particular links on page use rel="nofollow" attribute on `<a>` tag instead of nofollow meta tag **or** To prevent a google-like search engine from following a link to a page on your own site, use the robots.txt `Disallow` rule.

`robots.txt`
```
User-agent: *
Disallow: /products/
```

Learn more about nofollow 
[Qualify your outbound links to Google](https://support.google.com/webmasters/answer/96569)  
[Create a robots.txt file](https://support.google.com/webmasters/answer/6062596)  
[Be careful who you link to](https://support.google.com/webmasters/answer/7451184#linkwithcaution)  
[Evolving “nofollow” – new ways to identify the nature of links](https://webmasters.googleblog.com/2019/09/evolving-nofollow-new-ways-to-identify.html)
