+++
title= "P15 - Sitemap URL Does Not Exist In Robots.txt"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 15
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Sitemap URL does not exist in robots.txt |
| Code | P15 |
| Description | If sitemap url is not present in robots.txt then this alert raise. |
| Level | Warning |


Make sure search engine can easily find you sitemap, for that good place to do that is robots.txt. also make sure sitemap is present on the root URL. check below example

`robots.txt`
```
User-agent: *
Disallow:
Sitemap: https://www.example.com/sitemap.xml
```

#### Do I need a sitemap?

**You might need a sitemap if:**
- Your site is really large.
- Your site has a large archive of content pages that are isolated or not well linked to each other. 
- Your site is new and has few external links to it.
- Your site has a lot of rich media content (video, images) or is shown in Google News.

**You might not need a sitemap if:**
- Your site is "small".
- You're on a simple site hosting service like Blogger.
- Your site is comprehensively linked internally.
- You don't have many media files (video, image) or news pages that you need to appear in the index.

Learn more about [sitemap](https://support.google.com/webmasters/answer/156184). 