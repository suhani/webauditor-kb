---
title: S01 - Website Is Not Secure With HTTPS test
date: 2020-04-21T09:13:28.000Z
description: description
draft: false
weight: 1
---
| Alert Properties | Alert Description                                                                        |
| ---------------- | ---------------------------------------------------------------------------------------- |
| Alert Name       | Website is not secure with HTTPS                                                         |
| Code             | S01                                                                                      |
| Description      | Website is not secure enough for user, user data might have changes to leak from website |
| Level            | Error                                                                                    |

This alert occurs when your website is not enabled with `HTTPS` secure protocol.

## What is the HTTPS protocol?

HTTPS (Hypertext Transfer Protocol Secure) is an internet communication protocol that protects the integrity and confidentiality of data between the user's computer and the site. Users expect a secure and private online experience when using a website. web search engines like google encourage you to adopt HTTPS in order to protect your users' connections to your website, regardless of the content on the site.

Data sent using HTTPS is secured via Transport Layer Security protocol (TLS), which provides `three` key layers of protection:

* **Encryption**: encrypting the exchanged data to keep it secure from eavesdroppers. That means that while the user is browsing a website, nobody can "listen" to their conversations, track their activities across multiple pages, or steal their information.
* **Data integrity**: data cannot be modified or corrupted during transfer, intentionally or otherwise, without being detected.
* **Authentication**: proves that your users communicate with the intended website. It protects against man-in-the-middle attacks and builds user trust, which translates into other business benefits.

To know more about HTTPS protocol check out google webmasters article [Secure your site with HTTPS](https://support.google.com/webmasters/answer/6073543?hl=en).

## How to make the website secure?

To create a secure connection to the site we need the SSL certificate. This certificate can be generated and obtained by a Certification Authority (CA). 
You can get a free SSL certificate from Let’s Encrypt, a popular CA that provides certificates in the interest of creating a safer Internet: <https://letsencrypt.org/>. For more info on SSL check this article [Get an SSL certificate for your domain](https://support.google.com/domains/answer/7630973)

## Advantages of using HTTPS over SEO

* It may increase your rankings.
* High traffic in your website
* More conversion rate of the user
* User data become encrypted while communicating over the website