+++
title= "P08 - Missing / Blank Title Tags"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 8
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Missing / blank title tags |
| Code | P08 |
| Description | These alert occurs when page title is either missing or blank |
| Level | Warning |

## What is a title tag? 
The title tag is the identity of the web page. titles are critical to giving users a quick insight into the content of a result and why it’s relevant to their query. It's often the primary piece of information used to decide which result to click on, so it's important to use high-quality titles on your web pages.

## Avoid missing/blank title tags
When search engine search for pages based user input query, first thing search engine checks is the title tag if the title tag is missing or black. it will not consider it as a result of that query.

**Best Practices**
  - Create unique titles for each page
  - Use brief, but descriptive titles

**Avoid**
  - keyword stuffing
  - Using a single title across all of your site's pages or a large group of pages.
  - Using extremely lengthy titles that are unhelpful to users.
  - Stuffing unneeded keywords in your title tags.

To know about title page read this article [Create good titles and snippets in Search Results](https://support.google.com/webmasters/answer/35624?hl=en)

Check these videos  
[How does Google choose titles for search results?](https://www.youtube.com/watch?v=L3HX_8BAhB4)  
[The anatomy of a search result](https://www.youtube.com/watch?v=MOfhHPp5sWs)  