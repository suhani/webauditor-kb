+++
title= "S05 - Favicon Not Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 5
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Favicon not found |
| Code | S05 |
| Description | Favicon is very useful when your search result is shown by search engine  |
| Level | Warning |

## What is a favicon?

Favicons are not directly responsible for your Search Engine Optimization, however, it is a small image displayed along with search results, it is fine that favicon does not exist for the site but it will increase the site's credibility. Ideally, a website’s favicon should be the same as the logo for the business.

## How to create a favicon?

Check out this article of [Define a favicon to show in search results](https://support.google.com/webmasters/answer/9290858).

## Why use favicon?

* Brand awareness: Favicons keep your business’ brand identity in front of a user’s eyes even when they’re not on your site.
* Credibility and trust: If your site is missing a favicon, it will be conspicuous in its absence. On the flip side of that, adding a favicon to your site will make your business look more professional, established, and reliable. 
* Increased Usability Of Site: Usability of a site correlates to higher search engine ranking. So, if having a favicon next to your website title on browser tabs, on bookmarks, on historical archives, and so on, helps a user save time, identify and browse your website without difficulties, they may have a rather minuscule but noteworthy role in SEO. 

## Online Website to generate favicon    
https://realfavicongenerator.net/
