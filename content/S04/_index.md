+++
title= "S04 - Robots.txt Not Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 4
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Rotots.txt not found |
| Code | S04 |
| Description | Robots.txt is the key for search engines which pages to crawl & which pages to avoid, add robots.txt to improve SEO  |
| Level | Error |


## What is a robots.txt?

A robots.txt file tells search engine crawlers which pages or files the crawler can or can't request from your site. This is used mainly to avoid overloading your site with requests; it is not a mechanism for keeping a web page out of Google. To keep a web page out of Google, you should use [noindex directives](https://developers.google.com/search/reference/robots_meta_tag), or password-protect your page.

robots.txt is used primarily to manage crawler traffic to your site, and usually to keep a page off Google, depending on the file type:

A robots.txt file lives at the root of your site. So, for site www.example.com, the robots.txt file lives at www.example.com/robots.txt. robots.txt is a plain text file that follows the [Robots Exclusion Standard](https://en.wikipedia.org/wiki/Robots_exclusion_standard#About_the_standard). A robots.txt file consists of one or more rules. Each rule blocks (or allows) access for a given crawler to a specified file path on that website.

For more information on robots.txt check out this article [Introduction to robots.txt](https://support.google.com/webmasters/answer/6062608)

## How to create robots.txt?

Check out this article which explains, how to create robots.txt and some general guidelines [Create a robots.txt file](https://support.google.com/webmasters/answer/6062596), you also can check out this [faqs](https://support.google.com/webmasters/answer/7424835) section. 

## Does my website need a robots.txt file?
No. When search engine bots like Googlebot visits a website, we first ask for permission to crawl by attempting to retrieve the robots.txt file. A website without a robots.txt file, robots meta tags, or X-Robots-Tag HTTP headers will generally be crawled and indexed normally.

**Note:** robots.txt is not a place to hide the pages of websites, it's a place for telling crawlers to crawl site wisely.