+++
title= "P22 - Missing Alt Attribute On Image Tag"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 22
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Missing alt attribute on image tag |
| Code | P22 |
| Description | If image have missing alt attribute then this alert rais |
| Level | Warning |

## What is the alt attribute in the image tag? 
The alt attribute tells search engine & user what is this image all about.

## How to resolve this?
If your alt attribute missing or blank then it's very difficult for search engine what is the image all about. by adding more context around images, results can become much more useful, which can lead to higher-quality traffic to your site. you can also add images and it's information in [sitemap](https://support.google.com/webmasters/answer/178636) 

