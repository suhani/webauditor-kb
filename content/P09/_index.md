+++
title= "P09 - Missing / Blank Meta Description Tags"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 9
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Missing / blank meta description tags |
| Code | P09 |
| Description | These alert occurs when page meta description is either missing or blank |
| Level | Warning |

## What is page description(meta description tag) ?
A page's description meta tag gives search engines a summary of what the page is about. 

A page's title maybe a few words or a phrase, whereas a page's description meta tag might be a sentence or two or even a short paragraph.

The search engine will sometimes use the <meta> description tag from a page to generate a search results snippet if the Search engine thinks it gives users a more accurate description than would be possible purely from the on-page content. They are like a pitch that convinces the user that the page is exactly what they're looking for. There's no limit on how long a meta description can be, but the search result snippets are truncated as needed, typically to fit the device width.

## Avoid missing/blank meta description tag
Based on meta description tag search engine will understand what this page all about if this tag is missing its makes harder for search engine. and search engines might skip it. so always give meta description tag to your web page

**Best Practice**
- Make sure that every page on your site has a meta description.
- Differentiate the descriptions for different pages, ex: if pages have the same content as the product details page.
- Accurately summarise the page content
- Use unique descriptions for each page

**Avoid**
  - Writing a description meta tag that has no relation to the content on the page.
  - Using generic descriptions like "This is a web page" or "Page about baseball cards".
  - Filling the description with only keywords.
  - Copying and pasting the entire content of the document into the description meta tag.
  - Using a single description meta tag across all of your site's pages or a large group of pages.

To know about meta duplicate page read this article [How to write meta description?](https://support.google.com/webmasters/answer/35624?hl=en)