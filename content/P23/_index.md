+++
title= "P23 - Images With 404 Status Code"
date= 2020-04-21T14:43:28+05:30
description = ""
draft= false
weight = 23
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Images with 404 status code |
| Code | P23 |
| Description | Images are unable to load or inaccessible |
| Level | Error |


## What are images with a 404 status code?
The images which can't be loaded or are inaccessible returns 404 error response and won’t impact your site’s search performance, but will not be indexed by google crawler and are categorized in [error status](https://support.google.com/webmasters/answer/7440203?hl=en#error-status). 

## How to resolve?
Resolve any broken link which might be causing the image to return 404 error.

In theory, 404s have an impact on rankings. But not the rankings of a whole site. If an image returns a 404 error code, it means it doesn’t exist, so google and other search engines will not index it. Having thousands and thousands of 404 images can `impact your website overall`.

Read more about [best practices](https://support.google.com/webmasters/answer/114016?hl=en) for images.
