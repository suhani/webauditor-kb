+++
title= "P13 - Long Meta Description Tags"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 13
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Long meta description tags |
| Code | P13 |
| Description | This alert occurs if the ideal length of the meta description tag greater than 160 characters |
| Level | Warning |

A page's description meta tag gives search engines a summary of what the page is about. A page's title maybe a few words or a phrase, whereas a page's description meta tag might be a sentence or two or even a short paragraph.

If this summary is long then it's not useful for users, users may not take quick deception and might confuse what this page all about, search engines like google show a limited summary of the page. so make sure the web page summary is less than 160 characters. 

Google says that
> There's no limit on how long a meta description can be, but the search result snippets are truncated as needed, typically to fit the device width.

Read this article
[Create good meta descriptions](https://support.google.com/webmasters/answer/35624#meta-descriptions)
[Create good titles and snippets in Search Results](https://support.google.com/webmasters/answer/35624)