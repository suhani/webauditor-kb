+++
title= "S03 - Sitemap Not Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 3
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Sitemap not found |
| Code | S03 |
| Description | A sitemap is a file where you provide information about the pages, videos, and other files on your site, and the relationships between them. |
| Level | Error |

## What is a sitemap ?

A sitemap is a file where you provide information about the pages, videos, and other files on your site, and the relationships between them. Search engines like Google read this file to more intelligently crawl your site. A sitemap tells Search engines which pages and files you think are important in your site and also provides valuable information about these files: for example, for pages, when the page was last updated, how often the page is changed, and any alternate language versions of a page.

A good XML sitemap acts as a road map of your website that leads Google to all your important pages. XML sitemaps can be good for SEO, as they allow Google to quickly find your essential website pages.

You can use a sitemap to provide information about specific types of content on your pages, including [video](https://support.google.com/webmasters/answer/80471) and [image](https://support.google.com/webmasters/answer/178636) content. For example:

* A sitemap video entry can specify the video running time, category, and age-appropriateness rating.
* A sitemap image entry can include the image subject matter, type, and license.

Search engine like google says that if your site’s pages are properly linked, google can usually discover most of your site. Even so, a sitemap can improve the crawling of larger or more complex sites or more specialized files.

## When is a sitemap helpful for search engines?    

* Your site is really large.
* Your site has a large archive of content pages that are isolated or not well linked to each other.
* Your site is new and has few external links to it.

For more information on sitemap check this article [Manage your sitemaps](https://support.google.com/webmasters/answer/156184)

**Note:** [Make your sitemap available to Google](https://support.google.com/webmasters/answer/183668?hl=en&ref_topic=4581190#addsitemap) by adding it to your robots.txt file or directly submitting it to Search Console.    

## Sitemap format & guidelines as per google

* Checkout this article [Sitemap formats & General sitemap guidelines](https://support.google.com/webmasters/answer/183668)

**Note:** Make sure `single sitemap` have `max limit of 50MB` (uncompressed) and `50,000 URLs`. If you have a larger file or more URLs, you will have to break your list into multiple sitemaps. You can optionally create a sitemap index file (a file that points to a list of sitemaps) and submit that single index file to Google. You can submit multiple sitemaps and/or sitemap index files to Google.

## Tools to generate a sitemap

* Check out this article [sitemap-generators](https://code.google.com/archive/p/sitemap-generators/wikis/SitemapGenerators.wiki), you can choose which is fit for needs.
