+++
title= "P14 - Allowed By Robots.txt But Not Exists In Sitemaps"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 14
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | Allowed by robots.txt but not exists in sitemaps |
| Code | P14 |
| Description | If the URL is not present in sitemap which is allowed by robots.txt then this alert raise. |
| Level | Warning |

Make sure all URL allowed by robots.txt should be included in the sitemap, due to this its make easy for a search engine to find, crawl & index the page. 

In the case of canonical URL, make important page URL in the sitemap, from that search engine will know that, which page has a higher weight.

Learn about [sitemap](https://support.google.com/webmasters/answer/156184?hl=en&ref_topic=4581190)
Learn about [canonical urls](https://support.google.com/webmasters/answer/139066)
