+++
title= "P18 - External Pages Not Found"
date= 2020-04-21T14:43:28+05:30
description = "description"
draft= false
weight = 18
+++

| Alert Properties | Alert Description |
| ------ | ------ |
| Alert Name | High response time of pages and images |
| Code | P18 |
| Description | The LCP score of the page is moderate or low |
| Level | Warning |

## What is LCP?
The Largest Contentful Paint (LCP) metric reports the render time of the largest content element visible within the viewport. The overall threshold should be at least 75th percentile of page loads, to target most of the users.

  - 0-2.5 seconds is considered fast.
  - 2.5 to 4 seconds is considered as moderate and displays warning.
  - over 4 seconds is considered slow and displays an error.

LCP takes into consideration `<img>` elements, `<image>` elements inside an `<svg>` element, `<video>` elements, Block-level elements.

Pages with lower LCP scores make the google search engine crawl fewer pages using their allocated crawl budget, and this may negatively affect the indexation.

## How to resolve?


  - Enable compression
  - Minify CSS, Java Script, and HTML
  - Reduce redirects
  - Improve server response time
  - Optimize images
  - Establish third-party connections early (example:`<link rel="preconnect" href="https://example.com">` )

## Tools to evaluate the speed of your site:



  - [Page Speed](http://code.google.com/speed/page-speed/)
  - [YSlow](http://developer.yahoo.com/yslow/)
  - [WebPagetest](http://www.webpagetest.org/)
  - [Webmaster Tools](https://www.google.com/webmasters/tools)


[Read more](https://web.dev/lcp/)